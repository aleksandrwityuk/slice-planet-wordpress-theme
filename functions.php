<?php
/**
 * tecnavia functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package tecnavia
 */

if ( ! function_exists( 'wi_masonry_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function wi_masonry_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on tecnavia, use a find and replace
         * to change 'tecnavia' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'wi_masonry', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'primary_navigation' => __('Primary Navigation', 'wi_masonry'),
            'top_navigation' => __('Top Navigation', 'wi_masonry')
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );


    }
endif;
add_action( 'after_setup_theme', 'wi_masonry_setup' );

/**
 * Enqueue scripts and styles.
 */
function wi_masonry_scripts() {

    wp_enqueue_style( 'after_setup-style', get_stylesheet_uri() );
    wp_enqueue_style( "after_setup-addstyle", get_template_directory_uri() . '/scss/addstyle.css');
    wp_enqueue_style( "after_setup-desktop-style", get_template_directory_uri() . '/scss/desktop.css', array(), '1.0', 'screen and (min-width:1024px)');
    wp_enqueue_script( 'after_setup-desktop', get_template_directory_uri() . '/js/desktop.js', array(), '1.0');
    wp_enqueue_script( 'after_setup-scripts', get_template_directory_uri() . '/js/scripts.js', array(), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'wi_masonry_scripts' );

## Отключает Гутенберг (новый редактор блоков в WordPress).
## ver: 1.2
if( 'disable_gutenberg' ){
    remove_theme_support( 'core-block-patterns' ); // WP 5.5

    add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );

    // отключим подключение базовых css стилей для блоков
    remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );

    // Move the Privacy Policy help notice back under the title field.
    add_action( 'admin_init', function(){
        remove_action( 'admin_notices', [ 'WP_Privacy_Policy_Content', 'notice' ] );
        add_action( 'edit_form_after_title', [ 'WP_Privacy_Policy_Content', 'notice' ] );
    } );
}

/*  My customizer panel cotrols sections settings*/
function wi_masonry_customize_register( $wp_customize ) {
    //All our sections, settings, and controls will be added here
    $wp_customize->add_section( 'wi_masonry_new_section_name' , array(
        'title'      => __( 'Phone, Social networks and Email', 'mytheme' ),
        'priority'   => 160,
    ) );
    /*Email*/
    $wp_customize->add_setting( 'customizer_setting_email', array(
        'type' => 'option',
        'default' => 'info@web-impression.ru',
    ) );
    $wp_customize->add_control('mycontrol_email', array(
        'type' => 'text',
        'label'        => __( 'Email', 'wi_masonry' ),
        'description' => __( 'Input your email here' ),
        'section'    => 'wi_masonry_new_section_name',
        'settings'   => 'customizer_setting_email',
    ) );

    /*VK*/
    $wp_customize->add_setting( 'customizer_setting_vk', array(
        'type' => 'option',
        'default' => 'vk.com/web__impression',
    ) );
    $wp_customize->add_control('mycontrol_vk', array(
        'type' => 'text',
        'label'        => __( 'VK', 'wi_masonry' ),
        'description' => __( 'Input your vk address here' ),
        'section'    => 'wi_masonry_new_section_name',
        'settings'   => 'customizer_setting_vk',
    ) );

    /*Skype*/
    $wp_customize->add_setting( 'customizer_setting_skype', array(
        'type' => 'option',
        'default' => 'web-impression',
    ) );
    $wp_customize->add_control('mycontrol_skyte', array(
        'type' => 'text',
        'label'        => __( 'Skype', 'wi_masonry' ),
        'description' => __( 'Input your skype login here' ),
        'section'    => 'wi_masonry_new_section_name',
        'settings'   => 'customizer_setting_skype',
    ) );

    /*Phone*/
    $wp_customize->add_setting( 'customizer_setting_phone', array(
        'type' => 'option',
        'default' => '8-916-786-81-05',
    ) );
    $wp_customize->add_control('mycontrol_phone', array(
        'type' => 'text',
        'label'        => __( 'Phone', 'wi_masonry' ),
        'description' => __( 'Input your phone number here' ),
        'section'    => 'wi_masonry_new_section_name',
        'settings'   => 'customizer_setting_phone',
    ) );

    /*DOT*/
    $wp_customize->add_setting( 'customizer_setting_dot', array(
        'type' => 'option',
        'default' => 'web-impression',
    ) );
    $wp_customize->add_control('mycontrol_dot', array(
        'type' => 'text',
        'label'        => __( 'Dot', 'wi_masonry' ),
        'description' => __( 'Input your dot text here' ),
        'section'    => 'wi_masonry_new_section_name',
        'settings'   => 'customizer_setting_dot',
    ) );

}
add_action( 'customize_register', 'wi_masonry_customize_register' );

/*----------------------------------------------------------------------------------*/
/* Register new post type 'Портфолио'
/*----------------------------------------------------------------------------------*/

function register_portfolio_post_type() {

    $slug = 'portfolior';
    register_post_type($slug,
        array(
            'labels' => array(
                'name' => esc_html__( 'Портфолио', 'wi_masonry' ),
                'singular_name' => esc_html__( 'White papers', 'wi_masonry' )
            ),
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'capability_type' => 'post',
            'query_var' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'has_archive' => true,
            'menu_position' => null,

            'rewrite' => array(
                'slug' => $slug,
            ),
            'supports'     => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            ),
        )
    );

}
add_action( 'init', 'register_portfolio_post_type' );

