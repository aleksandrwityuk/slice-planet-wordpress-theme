<?php get_header(); ?>
		<!-- content -->
		<main class="content">		
			<!-- top section -->
			<section class="inner-top">
				<div class="wrapper">
					<h1>Разработка сайта для компании «Consulting group»</h1>
				</div>
			</section>
			<!-- end top section -->
			<section class="section-cols">
				<div class="wrapper">	
					<div class="cols">
                        <?php
                            $args = array(
                                'order' => 'ASC'
                            );
                            query_posts($args);
                        ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="col">
							<h4><img src="<?php the_post_thumbnail_url(); ?>" alt=""/><?php echo get_the_title();?></h4>
							<?php echo get_the_content(); ?>
						</div>
                        <?php endwhile; wp_reset_postdata(); endif; ?>
					</div>
				</div>	
			</section>
			<section class="section-preview">
				<div class="preview">
					<div class="pols">
                        <?php
                        $the_query = new WP_Query( array(
                            'post_type'      => 'portfolior',
                            'post_status'    => 'publish',
                            'order' => 'ASC'
                        ) );
                        ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
                                <div class="pol pol1"><?php echo get_the_content(); ?></div>
                        <?php endwhile; wp_reset_postdata(); ?>
					</div>
				</div>	
			</section>
			<section class="section-apply">
				<div class="apply">
					<h2>ОСТАВИТЬ ЗАЯВКУ</h2>
                    <?php echo do_shortcode( '[contact-form-7 id="36" title="Contact Form Task"]' ); ?>
				</div>	
			</section>
			
		</main>
		<!-- end content -->
<?php get_footer();

		
