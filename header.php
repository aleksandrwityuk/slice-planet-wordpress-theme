<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tecnavia
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Wi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <?php wp_head(); ?>
</head>
<body class="inner">
<div class="main-wrapper">
    <div class="container">
        <!-- header -->
        <header id="header">
            <div class="wrapper">
                <a href="#" class="logo"><img src="<?php echo get_template_directory_uri() . '/img/logo.png?ver1.0'?>" alt=""/></a>
                <a class="mobile-button" href="#"><span></span></a>
                <nav class="nav">
                    <?php
                    if (has_nav_menu('top_navigation')) :
                        wp_nav_menu(array(
                            'theme_location' => 'top_navigation',
                            'menu_class' => 'nav',
                            'container'         => 'false'
                        ));
                    endif;
                    ?>
                    <div class="soc">
                        <a href="#"><img src="img/soc1.png?ver1.0" alt=""/></a>
                        <a href="#"><img src="img/soc2.png?ver1.0" alt=""/></a>
                    </div>
                </nav>
                <div class="head-phone">
                    <a href="tel:89167868105" class="phone-link"><?php echo get_option( 'customizer_setting_phone', '' ); ?></a><br/>
                    <a href="#pop-up1" class="call-ord fancy"><i><img src="img/ico1.png?ver1.0" alt=""/></i><span>ЗАКАЗАТЬ ЗВОНОК</span></a>
                </div>
            </div>
        </header>
        <!-- end header -->