<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tecnavia
 */

?>

<!-- footer -->
<footer id="footer">
    <section class="sec-cont">
        <div class="contacts">
            <h2>контакты</h2>
            <div class="contact-item contact-phone">
                <a href="tel:89167868105"><i><img src="<?php echo get_template_directory_uri() . '/img/ico2.png?ver1.0' ?>" alt=""/></i><span><?php echo get_option( 'customizer_setting_phone', '' ); ?></span></a>
            </div>
            <div class="contact-item contact-links">
                <div class="contact-link"><a href="mailto:info@web-impression.ru"><i><img src="<?php echo get_template_directory_uri() . '/img/soc3.png?ver1.0' ?>" alt=""/></i><span><?php echo get_option( 'customizer_setting_email', '' ); ?></span></a></div>
                <div class="contact-link"><a href="#"><i><img src="<?php echo get_template_directory_uri() . '/img/soc4.png?ver1.0' ?> " alt=""/></i><span><?php echo get_option( 'customizer_setting_skype', '' ); ?></span></a></div>
                <div class="contact-link"><a href="#"><i><img src="<?php echo get_template_directory_uri() . '/img/soc5.png?ver1.0'?>" alt=""/></i><span><?php echo  get_option( 'customizer_setting_vk', '' ); ?></span></a></div>
                <div class="contact-link"><a href="#"><i><img src="<?php echo get_template_directory_uri() . '/img/soc6.png?ver1.0' ?>" alt=""/></i><span><?php echo  get_option( 'customizer_setting_dot', '' ); ?></span></a></div>
            </div>
            <a href="#pop-up2" class="btn btn-green fancy">ЗАКАЗАТЬ КОНСУЛЬТАЦИЮ</a>
        </div>
    </section>
    <div class="foot-bot">
        <div class="foot-logo"><a href="#"><img src="img/logo.png?ver1.0" alt=""/></a></div>
            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(array(
                    'theme_location' => 'primary_navigation',
                    'menu_class' => 'nav',
                    'container'         => 'div',
                    'container_class' => 'foot-nav'
                ));
            endif;
            ?>
    </div>
</footer>
<!-- end footer -->

<div class="box box0" data-scroll-speed="2"><img src="<?php echo get_template_directory_uri() . '/img/b12.png?ver1.0' ?>" alt=""/></div>
<div class="box box9" data-scroll-speed="6"><img src="<?php echo get_template_directory_uri() . '/img/b9.png?ver1.0'?>" alt=""/></div>
<div class="box box10" data-scroll-speed="5"><img src="<?php echo get_template_directory_uri() . '/img/b10.png?ver1.0'?>" alt=""/></div>
<div class="box box11" data-scroll-speed="2"><img src="<?php echo get_template_directory_uri() . '/img/b11.png?ver1.0'?>" alt=""/></div>
<div class="box box12" data-scroll-speed="3"><img src="<?php echo get_template_directory_uri() . '/img/b2.png?ver1.0'?>" alt="" width="75"/></div>
<div class="box box13" data-scroll-speed="3"><img src="<?php echo get_template_directory_uri() . '/img/b2.png?ver1.0'?>" alt="" width="66"/></div>
<div class="box box14" data-scroll-speed="3"><img src="<?php echo get_template_directory_uri() . '/img/b2.png?ver1.0'?>" alt=""/></div>
</div>
</div>

<div style="display:none;">
    <div class="pop-up" id="pop-up1">
        <h2>ЗАКАЗАТЬ ЗВОНОК</h2>
        <p>Заполните форму ниже, и мы свяжемся с Вами в&nbsp;ближайшее время</p>
        <div class="p-form">
            <?php echo do_shortcode( '[contact-form-7 id="37" title="Contact Form Pop Up"]' ); ?>
        </div>
    </div>
    <div class="pop-up" id="pop-up2">
        <h2>ЗАКАЗАТЬ КОНСУЛЬТАЦИЮ</h2>
        <p>Для отправки заявки на консультацию <br/>заполните форму ниже</p>
        <div class="p-form">
            <?php echo do_shortcode( '[contact-form-7 id="37" title="Contact Form Pop Up"]' ); ?>
        </div>
    </div>
</div>

<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!-- scripts -->
<?php wp_footer(); ?>
<script type="text/javascript">
    enquire.register("screen and (min-width: 1000px)", {
        match : function() {
            loadJS('<?php echo get_template_directory_uri();?> /js/desktop.js?ver1.0');
        }
    });
</script>

</body>
</html>